/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file libusb1-connector.c
 * @brief Connector to the USB clock via libusb1
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>

#include <libusb.h>	/* this is libusb1, see http://libusb.sourceforge.net/ */

#include "clockread.h"

/*
 * Starting the main program with LIBUSB_DEBUG not equal to 0 will output further
 * debug information from the usb library (if not disabled at its buildtime).
 */

static libusb_context *context;
static struct libusb_device *device;
static int init_done = 0;

/**
 * Search for and open the AVR Doper
 * @param[in] vendor Numeric vendor ID
 * @param[in] vendor_name Manufacturer name string to match or NULL
 * @param[in] product Numeric product ID
 * @param[in] product_name Product name string to match or NULL
 * @return Valid handle or NULL in the case of failure
 */
static void *open_usb_prog(int vendor, const char *vendor_name, int product, const char *product_name)
{
	struct libusb_device **list;
	struct libusb_device_descriptor desc;
	struct libusb_device_handle *handle = NULL;
	unsigned char device_string[128];
	int vendor_string_valid = 0, product_string_valid = 0;
	ssize_t cnt, i = 0;
	int err = 0, no_perm = 0;

	if (init_done == 0) {
		err = libusb_init(&context);
		if (err != 0) {
			syslog(LOG_ERR, "Cannot init the libusb. Giving up.");
			return NULL;
		}
#ifdef DEBUG
		/*
		 * Note: You can also set the LIBUSB_DEBUG environment
		 * variable to control the debug level.
		 */
		libusb_set_debug(context, 3);
#endif
		init_done = 1;
	}


	cnt = libusb_get_device_list(context, &list);
	if (cnt < 0) {
		syslog(LOG_NOTICE, "No USB devices connected.");
		return NULL;
	}

	if (cnt == 0) {
		libusb_free_device_list(list, 1);
		syslog(LOG_NOTICE, "No USB devices connected.");
		return NULL;
	}

	for (i = 0; i < cnt; i++) {
		/* check this one */
		device = list[i];

		err = libusb_get_device_descriptor(device, &desc);
		if (err != 0) {
			syslog(LOG_WARNING, "Cannot read device descriptor.");
			continue;
		}

		if (desc.idVendor == vendor && desc.idProduct == product) {

			/* This can be a candidate for our query */
			err = libusb_open(device, &handle);
			if (err != 0) {
				if (err == LIBUSB_ERROR_ACCESS)
					no_perm++;
				continue; /* skip silently. Maybe there is another one we can try */
			}

			if (vendor_name == NULL && product_name == NULL) {
				/* this one we will use */
				break;
			}

			/*
			 * Additional information was given. Check if they match.
			 */
			if (vendor_name != NULL) {
				err = libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, device_string, sizeof(device_string));
				if (err != 0) {
					if (strcmp(vendor_name, (char*)device_string) == 0)
						vendor_string_valid = 1;
				}
			} else
				vendor_string_valid = 1;	/* always true if not given */

			if (product_name != NULL) {
				err = libusb_get_string_descriptor_ascii(handle, desc.iProduct, device_string, sizeof(device_string));
				if (err != 0) {
					if (strcmp(product_name, (char*)device_string) == 0)
						product_string_valid = 1;
				}
			} else
				product_string_valid = 1;	/* always true if not given */

			if (product_string_valid == 1 && vendor_string_valid == 1)
				break;

			/* try the next one in the list */
			libusb_close(handle);
			handle = NULL;
		}
	}

	if (handle == NULL) {
		if (no_perm != 0)
			syslog(LOG_NOTICE, "Note: %d device(s) was/where skipped due to insufficient permissions.", no_perm);
		device = NULL;
	} else {
		/*
		 * Increase reference count to avoid destruction of the device
		 * object when we close it for a re-connect
		 */
		libusb_ref_device(device);
		/*
		err = libusb_kernel_driver_active(handle, 0);
		if (err > 0)
			err = libusb_detach_kernel_driver(handle, 0);
		*/
	}

	libusb_free_device_list(list, 1);
	return handle;
}

/* ------------------------------------------------------------------------- */

static const char timeout_message[]="Timeout";
static const char pipe_message[]="Broken pipe";
static const char device_message[]="Device disconnected";
static const char unknown_message[]="Unknown error";

const char *error_usb_clock(const int error_no)
{
	switch (error_no) {
	case -ETIMEDOUT:
		return timeout_message;
	case -EPIPE:
		return pipe_message;
	case LIBUSB_ERROR_NO_DEVICE:
		return device_message;
/*	case LIBUSB_ERROR_OTHER: */ /* libusb1 outputs: "unrecognised status code..." */
	}

	return unknown_message;
}

int open_usb_clock(void **handle)
{
	*handle = open_usb_prog(USB_VENDOR_ID, USB_VENDOR_STRING, USB_DEVICE_ID, USB_DEVICE_STRING);
	if (*handle == NULL)
		return -1;

	return 0;
}

/* FIXME seems broken, hangs on close or unref */
int reconnect_usb_clock(void **handle, int be_silent)
{
	int i, err;

	if (be_silent == 0)
		syslog(LOG_ERR, "Trying to reconnect...");

	libusb_close(*handle);
	if (device != NULL) {
		libusb_unref_device(device);
		device = NULL;
	}

	for (i = 0; i < 20; i++) {
		err = open_usb_clock(handle);
		if (err == 0)
			break;
		sleep(1);  /* 1 second */
	}

	if (be_silent == 0) {
		if (err == 0)
			syslog(LOG_NOTICE, "Reconnected.");
		else
			syslog(LOG_ERR, "Cannot re-connect. Giving up.");
	}

	return err;
}

void close_usb_clock(void *handle)
{
	if (handle != NULL)
		libusb_close(handle);
	if (device != NULL) {
		libusb_unref_device(device);
		device = NULL;
	}
	if (init_done != 0) {
		libusb_exit(context);
		context = NULL;
	}
}

int read_usb_clock(void *handle, void *buffer, int len, int timeout)
{
	/*
	 * Send a request of the following type to the USB clock:
	 *
	 * 8 bytes are received in the usbFunctionSetup() at clock's side:
	 *
	 * uchar       bmRequestType;
	 * uchar       bRequest;
	 * usbWord_t   wValue;
	 * usbWord_t   wIndex;
	 * usbWord_t   wLength;
	 *
	 * At clock's side its described in "struct usbRequest" = "usbRequest_t"
	 *
	 * Values for bRequest:
	 * 0x01:   GET_REPORT
	 * 0x02:   GET_IDLE
	 * 0x03:   GET_PROTOCOL
	 * 0x04-0x08:    Reserved
	 * 0x09:   SET_REPORT
	 * 0x0A:   SET_IDLE
	 * 0x0B:   SET_PROTOCOL
	 */
	return libusb_control_transfer(handle,
	/*	         (= 0x20)                   (=0x00)                (=0x80) */
		LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,	/* -> bmRequestType */
		USBRQ_HID_GET_REPORT,	/* -> bRequest */
		0, /* -> wValue */
		0, /* -> wIndex */
		buffer,
		(uint16_t)len, /* -> wLength */
		(unsigned)timeout);
}

/* ------------------------------------------------------------------------- */
