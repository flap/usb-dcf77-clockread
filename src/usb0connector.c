/*
 * This code is based on:
 *
 * Project: AVR Programmer
 * Author: Christian Starkjohann
 * Creation Date: 2006-07-10
 * Tabsize: 4
 * Copyright: (c) 2006 by OBJECTIVE DEVELOPMENT Software GmbH
 * License: GNU GPL v2 (see License.txt) or proprietary (CommercialLicense.txt)
 *
 * Changes made:
 *
 * - clean up and re-format
 * - avoid camel case
 */

/**
 * @file usb0connector.c
 * @brief Connector to the USB clock via libusb0
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>

#include <usb.h>    /* this is libusb, see http://libusb.sourceforge.net/ */

#include "clockread.h"

/* ------------------------------------------------------------------------- */

#define USB_ERROR_NOTFOUND  1
#define USB_ERROR_ACCESS    2
#define USB_ERROR_IO        3

static int usb_open_device(void **device, int vendor, char *vendor_name, int product, char *product_name)
{
	struct usb_bus      *bus;
	struct usb_device   *dev;
	usb_dev_handle      *handle = NULL;
	int                 errorCode = USB_ERROR_NOTFOUND;
	static int          didUsbInit = 0;

	if (!didUsbInit){
		didUsbInit = 1;
		usb_init();
	}
	usb_find_busses();
	usb_find_devices();
	for (bus=usb_get_busses(); bus; bus=bus->next) {
		for (dev=bus->devices; dev; dev=dev->next) {
			if (dev->descriptor.idVendor == vendor && dev->descriptor.idProduct == product) {
				char    string[256];
				int     len;

				handle = usb_open(dev); /* we need to open the device in order to query strings */
				if (!handle) {
					errorCode = USB_ERROR_ACCESS;
					syslog(LOG_WARNING, "Cannot open USB device: %s", usb_strerror());
					continue;
				}
			/* name does not matter? */
				if (vendor_name == NULL && product_name == NULL)
					break;

			/* now check whether the names match: */
				len = usb_get_string_simple(handle, dev->descriptor.iManufacturer, string, sizeof(string));
				if (len < 0) {
					errorCode = USB_ERROR_IO;
					syslog(LOG_WARNING, "Cannot query manufacturer for device: %s", usb_strerror());
				} else {
					errorCode = USB_ERROR_NOTFOUND;
					if (strcmp(string, vendor_name) == 0) {
						len = usb_get_string_simple(handle, dev->descriptor.iProduct, string, sizeof(string));
						if (len < 0) {
							errorCode = USB_ERROR_IO;
							syslog(LOG_WARNING, "Cannot query product for device: %s", usb_strerror());
						} else {
							errorCode = USB_ERROR_NOTFOUND;
							if (strcmp(string, product_name) == 0)
							break;
						}
					}
				}
				usb_close(handle);
				handle = NULL;
			}
		}
		if (handle)
			break;
	}

	if (handle != NULL) {
		errorCode = 0;
		*device = handle;
	}

	return errorCode;
}

/* ------------------------------------------------------------------------- */

static const char timeout_message[]="Timeout";
static const char pipe_message[]="Broken pipe";
static const char device_message[]="Device disconnected";
static const char unknown_message[]="Unknown error";

const char *error_usb_clock(const int error_no)
{
	switch (error_no) {
	case -ETIMEDOUT:
		return timeout_message;
	case -EPIPE:
		return pipe_message;
	case -4: /*LIBUSB_ERROR_NO_DEVICE */
		return device_message;
/*	case LIBUSB_ERROR_OTHER */ /* libusb1 outputs: "unrecognised status code..." */
	}

	return unknown_message;
}

int open_usb_clock(void **device)
{
	return usb_open_device(device, USB_VENDOR_ID, USB_VENDOR_STRING, USB_DEVICE_ID, USB_DEVICE_STRING);
}

int reconnect_usb_clock(void **device, int be_silent)
{
	int i, err;

	if (be_silent == 0)
		syslog(LOG_ERR, "Trying to reconnect...");

	if (*device != NULL) {
		usb_close(*device);
		*device = NULL;		/* mark 'device' as invalid? */
	}
	for (i = 0; i < 20; i++) {
		err = open_usb_clock(device);
		if (err == 0)
			break;
		sleep(1);  /* wait before trying again */
	}

	if (be_silent == 0) {
		if (err == 0)
			syslog(LOG_NOTICE, "Reconnected.");
		else
			syslog(LOG_ERR, "Cannot re-connect USB device. Giving up.");
	}

	return err;
}

void close_usb_clock(void *device)
{
	if (device != NULL)
		usb_close(device);
}

int read_usb_clock(void *handle, void *buffer, int len, int timeout)
{
	/*
	 * Send a request of the following type to the USB clock:
	 *
	 * 8 bytes are received in the usbFunctionSetup() at clock's side:
	 *
	 * uchar       bmRequestType;
	 * uchar       bRequest;
	 * usbWord_t   wValue;
	 * usbWord_t   wIndex;
	 * usbWord_t   wLength;
	 *
	 * At clock's side its described in "struct usbRequest" = "usbRequest_t"
	 *
	 * Values for bRequest:
	 * 0x01:   GET_REPORT
	 * 0x02:   GET_IDLE
	 * 0x03:   GET_PROTOCOL
	 * 0x04-0x08:    Reserved
	 * 0x09:   SET_REPORT
	 * 0x0A:   SET_IDLE
	 * 0x0B:   SET_PROTOCOL
	 */
	return usb_control_msg(handle,
	/*	  (= 0x20)           (=0x00)          (=0x80) */
		USB_TYPE_CLASS | USB_RECIP_DEVICE | USB_ENDPOINT_IN,	/* -> bmRequestType */
		USBRQ_HID_GET_REPORT,	/* -> bRequest */
		0,		/* -> wValue = ReportType (highbyte), ReportID (lowbyte) */
		0,		/* -> wIndex */
		buffer,		/* FIXME */
		len,		/* -> wLength */
		timeout);	/* FIXME */
}

/* ------------------------------------------------------------------------- */
