/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Note: This file will be included by dcf77_decoder.c */

/**
 * @file clock.c
 * @brief The internal clock fed by the DCF receiver
 */

/** days of each month (0...11) */
static const int mday[] = {
	31, /* January */
	28, /* February */
	31, /* March */
	30, /* April */
	31, /* May */
	30, /* June */
	31, /* July */
	31, /* August */
	30, /* September */
	31, /* Octopber */
	30, /* November */
	31, /* December */
};

static void feed_next_year(struct usbdcf_data *instance)
{
	instance->reference_time.tm_year++;
}

static void feed_next_month(struct usbdcf_data *instance)
{
	instance->reference_time.tm_mon++;
	if (instance->reference_time.tm_mon >= 12) {
		feed_next_year(instance);
		instance->reference_time.tm_mon = 0;
	}
}

static int is_leap_year(int year)
{
	year += 1900;

	if (year % 100 == 0 && year % 400 == 0)
		return 1;
	else {
		if (year % 4 == 0)
			return 1;
	}

	return 0;
}

static void feed_next_day(struct usbdcf_data *instance)
{
	int dayofmonth;

	instance->reference_time.tm_mday++;
	if (instance->reference_time.tm_mday > mday[instance->reference_time.tm_mon]) {
		/*
		 * handle the leap year right now
		 */
		if (instance->reference_time.tm_mon == 1) { /* february? */
			dayofmonth = mday[instance->reference_time.tm_mon];
			if (is_leap_year(instance->reference_time.tm_year))
				dayofmonth++;

			if (instance->reference_time.tm_mday <= dayofmonth)
				return;
		}

		feed_next_month(instance);
		instance->reference_time.tm_mday = 1; /* this one must be 1 */
	}
}

static void feed_next_hour(struct usbdcf_data *instance)
{
	instance->reference_time.tm_hour++;
	if (instance->reference_time.tm_hour >= 24) {
		feed_next_day(instance);
		instance->reference_time.tm_hour = 0;
		/* reset the leap second counter (paranoia) */
		instance->leap_second_count = 0;
	}
}

static void feed_next_minute(struct usbdcf_data *instance)
{
	instance->reference_time.tm_min++;
	if (instance->reference_time.tm_min >= 60) {
		feed_next_hour(instance);
		instance->reference_time.tm_min = 0;
	}
}

static void timerdiv(struct timeval *val, unsigned div)
{
	unsigned long long usec;

	usec = val->tv_sec;
	usec *= 1000000U;
	usec += val->tv_usec;

	usec /= div;

	val->tv_sec = usec / 1000000U;
	val->tv_usec = usec - (val->tv_sec * 1000000U);
}

/**
 * Feed the next second to our reference time
 * @param[inout] instance all we need to do our job here
 *
 * Forward the time to the ntpd each 10 seconds
 */
static void feed_next_second(struct usbdcf_data *instance)
{
	unsigned counter_period;
	struct timeval diff;

	instance->reference_time.tm_sec++;
	/*
	 * One hour prior the leap second, we receive a signal
	 * But important only is the last minute of an hour.
	 */
	if (instance->leap_second != 0) {
		if (instance->reference_time.tm_min == 59) {
			if (instance->reference_time.tm_sec >= 61) {
				instance->leap_second = 0; /* done */
				/* reset the leap second counter */
				instance->leap_second_count = 0;
			} else
				return;
		}
	} else {
		/* if an hour contains more than 20 active leap second markers we expect a leap second */
		if (instance->leap_second_count > 19) {
			instance->leap_second = 1;
			syslog(LOG_NOTICE, "We expect a leap second at the end of this hour");
		}
	}

	if (instance->reference_time.tm_sec >= 60) {
		feed_next_minute(instance);
		instance->reference_time.tm_sec = 0;
	}
#ifdef DEBUG
	fprintf(stderr,"%02d:%02d:%02d - % 2d-%02d-%04d\n",
		instance->reference_time.tm_hour,
		instance->reference_time.tm_min,
		instance->reference_time.tm_sec,
		instance->reference_time.tm_mday,
		instance->reference_time.tm_mon + 1,
		instance->reference_time.tm_year + 1900);
#endif
	if (instance->reference_time.tm_sec % 10 == 0) {
	/*
	 * We expect the interval count is between 1,001 ... 1,000,000
	 * So, we expect we can calculate in "microseconds"
	 */
		if (instance->data.cur_interval < 1001) {
			syslog(LOG_ERR, "Interval count not above expected values. Cannot continue!");
			die_hard();
			return;	/* there is no way to continue */
		}
	/* we want to calculate with one decimal place */
		counter_period = 10000000U / instance->data.cur_interval;
	/*
	 * Timestamp meaning in the USB report:
	 *
	 *                                     cur_interval
	 *            |<---------------------------------------------------------------->|
	 *  ref ------|------------------------------------------------------------------|-------------------------
	 *  dcf ------------------------------------|--------------------------------------------------------------
	 *                                          |----------------------------------->|
	 *                                                      cur_phase
	 *                                                                               |--------------------->|
	 *                                                                                    stamp_offset      ^
	 *                                                                                                      |
	 *                                                  this is the point of time we got in the report _____|
	 *
	 * Everything is based in timer counts. Base timing is the "current interval":
	 * Its given in counts per second. It should give a timing in [�s] for the
	 * calculation.
	 */
		instance->usec_offset = ((instance->data.cur_phase + instance->data.stamp_offset) * counter_period);
		instance->usec_offset /= 10;	/* now its [�s] again */

		/* we assume the clock reads the time in the middle of our USB query */
		timersub(&instance->post_time_stamp, &instance->pre_time_stamp, &diff);
		timerdiv(&diff, 2);
		timeradd(&instance->pre_time_stamp, &diff, &instance->read_time_stamp);

		/*
		 * only report if we are knowing the time
		 */
		if (instance->clock_is_set != 0)
			report_time(instance);
	}
}

/**
 * Feed the DCF time into our timing system
 * @param[inout] instance all we need to do our job here
 */
static void feed_valid_time(struct usbdcf_data *instance)
{
	struct tm dcf_ref_time;
	time_t current_time, internal_time;

	dcf_ref_time.tm_sec = 0;	/* 0...60 */
	dcf_ref_time.tm_min = instance->current_time.minute;	/* 0...59 */
	dcf_ref_time.tm_hour = instance->current_time.hour;	/* 0...23 */
	dcf_ref_time.tm_mday = instance->current_time.day;	/* 1...31 */
	dcf_ref_time.tm_mon = instance->current_time.month - 1;	/* 0...11 */
	dcf_ref_time.tm_year = 2000 + instance->current_time.year - 1900;
	dcf_ref_time.tm_wday = 0;	/* ignored */
	dcf_ref_time.tm_yday = 0;	/* ignored */
	dcf_ref_time.tm_isdst = 0;

	current_time = mktime(&dcf_ref_time);
	if (instance->current_time.summer_time)
		current_time -= 3600; /* CEST */
	current_time -= 3600;	/* CET */
	/* convert back */
	gmtime_r(&current_time, &dcf_ref_time);

	if (instance->clock_is_set == 0) {
		instance->reference_time = dcf_ref_time;
		instance->reference_time.tm_sec = -1;
		instance->clock_is_set = 1;
		syslog(LOG_NOTICE, "Reference time set by DCF time");
	} else {
		/* what we think the correct time is */
		internal_time = mktime(&instance->reference_time);
		/*
		 * we are always a half second behind.
		 */
		internal_time++;
		/* if we are too late, correct one second per minute */
		if (internal_time < current_time) {
			syslog(LOG_WARNING, "Too late. Skip one second");
			feed_next_second(instance);
		}
		/* we are too early, expand a second per minute */
		if (internal_time > current_time) {
			syslog(LOG_WARNING, "Too early. Expand one second");
			/* FIXME do not decrement a 0! */
			instance->reference_time.tm_sec--;
		}
	}
}
