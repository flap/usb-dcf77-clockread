/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file clockread.c
 * @brief Main routines
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "clockread.h"

static struct usbdcf_data dev;
static int shut_down;
#ifdef DEBUG
static int debug = 1;
#else
static int debug = 0;
#endif

/*
 * TODO Add other signals to get a life report of most important internal
 * states
 */
static void terminate(int sig)
{
	shut_down = 1;
	syslog(LOG_NOTICE, "SIGTERM received");
}

void die_hard(void)
{
	shut_down = 1;
	syslog(LOG_NOTICE, "Die hard...");
}

static void usage(const char *prg)
{
	printf("usage: %s [options]\n", prg);
	printf(" '-d' don't disappear into the background\n");
	printf(" '-v' print program version and exit\n");
	printf(" '-h' print this help and exit\n");
}

int main(int argc, char **argv)
{
	int err = 0, c;
	pid_t pid, sid;

	/* handle command line options first */
	while (1) {
		c = getopt(argc, argv, "hvd");
		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug = 1;
			break;
		case 'h':
			usage(argv[0]);
			exit(0);
		case 'v':
			printf("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
			exit(0);
		}
	}

	/* ----------- becomming a daemon -------------- */
	if (debug == 0) {
		pid = fork();
		if (pid < 0)
			exit(EXIT_FAILURE);
		if (pid > 0)
			exit(EXIT_SUCCESS);
	}

	umask(0);

	if (debug == 0) {
		openlog(PACKAGE, LOG_ODELAY, LOG_USER);

		sid = setsid();
		if (sid < 0) {
			syslog(LOG_ERR, "Error: Cannot create session ID");
			exit(EXIT_FAILURE);
		}
	
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
	} else
		openlog(PACKAGE, LOG_PERROR | LOG_ODELAY, LOG_USER);

	/* ----------- do the work -------------- */

	err = open_usb_clock(&dev.usb_device);
	if ( err != 0) {
		syslog(LOG_ERR, "Error: USB clock not found");
		goto error_on_usb;
	}

	syslog(LOG_NOTICE, "Found USB clock DCF77-Clock, Vendor: kreuzholzen.de");

	err = init_ntp_connection(&dev, 0);	/* FIXME provide unit_number to be set by command line parameter */
	if (err != 0) {
		syslog(LOG_ERR, "Error: Cannot init ntpd connection");
		goto error_on_ntp;
	}

	err = init_dcf77_decoder(&dev);
	if (err != 0) {
		syslog(LOG_ERR, "Error: Cannot init dcf77 decoder");
		goto error_on_dcf;
	}

	signal(SIGTERM, terminate);

	syslog(LOG_NOTICE, "Entering DCF77 decoding and reporting loop");
	while (shut_down == 0) {
		gettimeofday(&dev.pre_time_stamp, NULL);
		/*
		 * TODO: Should we better wait 500ms only? After 500ms the next
		 * query should occur.
		 */
		err = read_usb_clock(dev.usb_device, &dev.data, sizeof(dev.data), 1000);
		if (err < 0 || err != sizeof(dev.data)) {
			syslog(LOG_ERR, "USB: Error reading data (%d=%s)\n", err, error_usb_clock(err));
			err = reconnect_usb_clock(&dev.usb_device, 0);
			if (err == 0)
				continue;	/* try again */
			break;	/* give up */
		}
#if 0 /* def DEBUG */
		printf("flags = %X, bit = %X, stamp_offset = %hX, cur_phase = %hX, cur_interval = %hX\n",
			dev.data.flags, dev.data.bit, dev.data.stamp_offset, dev.data.cur_phase, dev.data.cur_interval);
#endif
		gettimeofday(&dev.post_time_stamp, NULL);
		feed_data(&dev);
		usleep(500000);
	}
	syslog(LOG_NOTICE, "Exiting gracefully");

	exit_dcf77_decoder(&dev);
error_on_dcf:
	exit_ntp_connection(&dev);
error_on_ntp:
	close_usb_clock(dev.usb_device);
error_on_usb:
	closelog();

	return err;
}
