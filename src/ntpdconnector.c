/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file ntpdconnector.c
 * @brief Connector to the ntp daemon via shared memory
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <syslog.h>

#include "clockread.h"

#define NTP_KEY 1314148400

struct shmTime {
        int    mode; /* 0 - if valid set: Use values, clear valid
                      * 1 - if valid set: If count before and after read of values is equal,
                      *     use values and clear valid
                      */
        int    count;
        time_t clockTimeStampSec;      /* external clock */
        int    clockTimeStampUSec;     /* external clock */
        time_t receiveTimeStampSec;    /* internal clock, when external value was received */
        int    receiveTimeStampUSec;   /* internal clock, when external value was received */
        int    leap;
        int    precision;
        int    nsamples;
        int    valid;
        int    dummy[10];
};

struct ntp_connector {
	int unit_number;
	int shmid;
	struct shmTime *pst;
};

static int open_shared_memory(struct ntp_connector *con)
{
	con->shmid = shmget(NTP_KEY + con->unit_number, sizeof(struct shmTime), IPC_CREAT);
	if (con->shmid == -1) {
		syslog(LOG_CRIT, "Error allocating shared memory");
		return -1;
	}

	con->pst = (struct shmTime*)shmat(con->shmid, NULL, 0);
	if (con->pst == (void *)-1) {
		syslog(LOG_CRIT, "Error attaching shared memory");
		return -1;
	}

	return 0;
}

static void close_shared_memory(struct ntp_connector *con)
{
	int rc;

	rc = shmdt(con->pst);
	if (rc != 0)
		syslog(LOG_ERR, "Error freeing shared memory");

	con->pst = NULL;
}

/* ----------------------------------------------------------------------- */

/**
 * Establish a connection to the ntpd
 * @param[inout] instance data collection
 * @param[in] unit_number number of this unit to connect to the ntpd
 * @return 0 on success, -1 on failure
 */
int init_ntp_connection(struct usbdcf_data *instance, int unit_number)
{
	struct ntp_connector *con;
	int rc;

	instance->ntp_connector = malloc(sizeof(struct ntp_connector));
	if (instance->ntp_connector == NULL) {
		syslog(LOG_CRIT, "Error allocating an ntp connector. Memory exhaused?\n");
		return -1;
	}

	con = (struct ntp_connector*)instance->ntp_connector;
	con->unit_number = unit_number;
	rc = open_shared_memory(con);
	if (rc != 0) {
		free(instance->ntp_connector);
		instance->ntp_connector = NULL;
		return -1;
	}

	/* Attention: Side effect due to shared memory! */
	con->pst->mode = 1;
	con->pst->valid = 0;

	return 0;
}

/**
 * Delete a connection to the ntpd
 * @param[inout] instance data collection
 */
void exit_ntp_connection(struct usbdcf_data *instance)
{
	struct ntp_connector *con = instance->ntp_connector;

	if (con != NULL) {
		close_shared_memory(con);
		free(con);
		instance->ntp_connector = NULL;
	}
}

/**
 * Feed the new time stamp to the NTP daemon
 * @param[inout] instance data collection
 */
int report_time(struct usbdcf_data *instance)
{
	struct ntp_connector *con = (struct ntp_connector*)instance->ntp_connector;

	/* important value */
	instance->reference_time.tm_isdst = 0;

	con->pst->clockTimeStampSec = mktime(&instance->reference_time);
	con->pst->clockTimeStampUSec = instance->usec_offset;
	if (con->pst->clockTimeStampUSec > 1000000) {
		con->pst->clockTimeStampSec++;
		con->pst->clockTimeStampUSec -= 1000000;
	}

	con->pst->receiveTimeStampSec = instance->read_time_stamp.tv_sec;
	con->pst->receiveTimeStampUSec = instance->read_time_stamp.tv_usec;
	con->pst->count++;
	con->pst->valid = 1;

#ifdef DEBUG
	fprintf(stderr, "Time is right now: (%d) %s, ", con->pst->clockTimeStampUSec, ctime(&con->pst->clockTimeStampSec));
	fprintf(stderr, "Measured at: %s\n", ctime(&con->pst->receiveTimeStampSec));
#endif
	return 0;
}
