/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file dcf77_decoder.c
 * @brief Main routines to receive the time via DFC77
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <syslog.h>
#include <time.h>

#include "clockread.h"

/*
From http://www.ptb.de/de/org/4/44/442/dcf_kode.htm:

Die Zonenzeitbits Z1 und Z2 (Sekundenmarken Nr. 17 und 18) zeigen an, auf
welches Zeitsystem sich die ab der Sekundenmarke 20 �bertragene
Zeitinformation bezieht. Bei der Aussendung von MEZ hat Z1 den Zustand Null
und Z2 den Zustand Eins. Bei der Aussendung von MESZ ist es umgekehrt.

Das Ank�ndigungsbit A1 (Nr. 16) weist auf einen bevorstehenden Wechsel des
Zeitsystems hin. Vor dem �bergang von MEZ nach MESZ oder zur�ck wird A1
jeweils eine Stunde lang im Zustand Eins ausgesendet: vor dem �bergang von
MEZ nach MESZ (MESZ nach MEZ) von 01:00:16 Uhr MEZ (02:00:16 Uhr MESZ) bis
01:59:16 Uhr MEZ (02:59:16 Uhr MESZ).

Mit dem Ank�ndigungsbit A2 (Nr. 19) wird auf das bevorstehende Einf�gen einer
Schaltsekunde aufmerksam gemacht. A2 wird ebenfalls eine Stunde lang vor dem
Einf�gen einer Schaltsekunde im Zustand Eins ausgestrahlt. Vor dem Einf�gen
einer Schaltsekunde am 1. Januar (1. Juli) wird A2 daher sechzig Mal von
00:00:19 Uhr MEZ (01:00:19 Uhr MESZ) bis 00:59:19 Uhr MEZ (01:59:19 Uhr MESZ)
im Zustand Eins ausgesendet.

From http://www.meinberg.de/german/info/leap-second.htm:
Das Einf�gen einer Schaltsekunde erfolgt immer am letzten Tag eines Monats,
vorzugsweise Ende Juni oder Ende Dezember, um Mitternacht UTC.

Again from http://www.ptb.de/de/org/4/44/442/dcf_kode.htm:
Das Einf�gen einer Schaltsekunde geschieht bei den AM-Sekundenmarken in
folgender Weise. Die der Marke 01:00:00 Uhr MEZ bzw. 02:00:00 Uhr MESZ
vorhergehende 59. Sekundenmarke wird anders als sonst mit einer Dauer von
0,1 s ausgesendet. Danach wird die eingef�gte 60. Sekundenmarke ohne
Tr�gerabsenkung ausgestrahlt. Die Wahrscheinlichkeit daf�r, Schaltsekunden
auslassen zu m�ssen, ist gering, die technischen Einrichtungen am Sender
lassen es jedoch zu.
*/

/* keep it separate for easier testing */
#include "clock.c"

/* ------------------------------------------------------------------------ */

#define DATE_UNKNOWN 1
#define DATE_IN_SYNC_1 2
#define DATE_IN_SYNC_2 4
#define DATE_KEEP_AN_EYE 8
#define DATE_KNOWN 16

#define TIME_UNKNOWN 1
#define TIME_IN_SYNC_1 2
#define TIME_IN_SYNC_2 4
#define TIME_KEEP_AN_EYE 8
#define TIME_KNOWN 16

#define TIME_SYNCED 1
#define DATE_SYNCED 2

/*
 * Updating the time
 *
 *                 56                57                58                59                 0
 * ref  ------------|-----------------|-----------------|-----------------|-----------------|--
 * dfc  ---|-----------------|-----------------|-----------------|-----------------|-----------
 *        57                58                59                 S                 0
 *         ^                     we get a new timestamp here ----^ >>>>>>>>>>>>>>>>>>>>>>>> ^-- and here it will be valid
 *         --- real time
 *
 * The reference time is always 'cur_phase' too late.
 *
 * When the clock reports the 'S' event, the decoder decodes the time of
 * the next minute.
 */

/*
 * DCF77 encoding (from http://www.ptb.de/de/org/4/44/442/dcf_kode.htm)
 *
 * - bit 0: always 0
 * ---------------------------------------------------
 * - bit 1...14 other usage
 * ---------------------------------------------------
 * - bit 15: call (failure in the transmitter)
 * ---------------------------------------------------
 * - bit 16: 1=at the end of this hour CET/CEST will be switched
 * - bit 17: 0=CET, 1=CEST
 * - bit 18: 0=CEST, 1=CET
 * - bit 19: 1=at the end of this hour a leap second will be added/removed
 * ---------------------------------------------------
 * - bit 20: always 1 (start of time info)
 * ---------------------------------------------------
 * - bit 21: 1=1 minute
 * - bit 22: 1=2 minutes
 * - bit 23: 1=4 minutes
 * - bit 24: 1=8 minutes
 * - bit 25: 1=10 minutes
 * - bit 26: 1=20 minutes
 * - bit 27: 1=40 minutes               (0...59)
 * - bit 28: minutes parity (even)
 * ---------------------------------------------------
 * - bit 29: 1=1 hour
 * - bit 30: 1=2 hour
 * - bit 31: 1=4 hour
 * - bit 32: 1=8 hour
 * - bit 33: 1=10 hour
 * - bit 34: 1=20 hour (0...23)
 * - bit 35: hour parity (even)
 * ---------------------------------------------------
 * - bit 36: 1=1 day of month
 * - bit 37: 1=2 day of month
 * - bit 38: 1=4 day of month
 * - bit 39: 1=8 day of month
 * - bit 40: 1=10 day of month
 * - bit 41: 1=20 day of month           (1...31)
 * ---------------------------------------------------
 * - bit 42: 1=1 day of week
 * - bit 43: 1=2 day of week (1=monday, 7=sunday)
 * - bit 44: 1=4 day of week
 * ---------------------------------------------------
 * - bit 45: 1=1 month
 * - bit 46: 1=2 month
 * - bit 47: 1=4 month
 * - bit 48: 1=8 month
 * - bit 49: 1=10 month                  (1...12)
 * ---------------------------------------------------
 * - bit 50: 1=1 year
 * - bit 51: 1=2 year
 * - bit 52: 1=4 year
 * - bit 53: 1=8 year
 * - bit 54: 1=10 year
 * - bit 55: 1=20 year
 * - bit 56: 1=40 year
 * - bit 57: 1=80 year                  (years since 2000)
 * - bit 58: date parity (even -> 22 bits, from 36 to 58)
 * ---------------------------------------------------
 * - bit 59: not send, sync signal (or 0 in case of a leap second)
 *
 * In the case of a leap second:
 * - bit 59 is sent as a regular 0
 * - bit 60 will be the sync signal in this (rare) case
 *
 * Prior any leap second, bit 19 will be sent as 1 the whole hour before the
 * leap second. Otherwise bit 19 is always 0.
 * Prior switching between CET and CEST bit 16 will be sent as 1 the whole
 * hour before the switch happens.
 */

/**
 * Stores one bit into the stream buffer
 * @param[in] instance data collection
 * @param[in] value encoded bit value
 */
static void store_next_bit(struct usbdcf_data *instance, unsigned value)
{
	assert(value < 256);
	instance->rec_buf[instance->current_index] = (unsigned char)value;
	instance->current_index++;
	if (instance->current_index > 60)
		instance->current_index = 60;
}

/**
 * @param[in] instance data collection
 * @param[in] number bit number to read
 */
static int read_bit(struct usbdcf_data *instance, size_t number)
{
	if (number > 60)
		return BIT_UNKNOWN;

	return instance->rec_buf[number];
}

/**
 * Check the parity of the given bitfield
 * @param[in] instance data collection
 * @param[in] positions start bit number
 * @param[in] count bits to check (includes the parity bit)
 * @return 0: Checksum okay, 1: checksum failure, 2: unable to calculate checksum due to bit failures
 */
static int checksum(struct usbdcf_data *instance, size_t positions, size_t count)
{
	size_t u;
	unsigned onec = 0;
	int rc;

	for (u = positions; u < (positions + count); u++) {
		rc = read_bit(instance, u);
		switch(rc) {
		case BIT_UNKNOWN:
			return 2;
		case BIT_1:
			onec++;
		}
	}
	/* parity is even */
	if (onec & 0x01){
		return 1;
	}

	return 0;
}

/**
 * Check if all bits are valid
 * @param[in] instance data collection
 * @param[in] positions start bit number
 * @param[in] count bits to check
 * @return 0: all are valid, 1: at least one was invalid
 */
static int check(struct usbdcf_data *instance, size_t positions, size_t count)
{
	size_t u;

	for (u = positions; u < (positions + count); u++) {
		if (read_bit(instance, u) == BIT_UNKNOWN)
			return 1;
	}

	return 0;
}

/**
 * Converts BCD notation into simple binary format
 * @param[in] instance data collection
 * @param[in] positions start bit number
 * @param[in] count bits to convert
 * @return decoded value
 */
static int decode_bcd(struct usbdcf_data *instance, size_t position, size_t count)
{
	static const int value[8] = { 1, 2, 4, 8, 10, 20, 40, 80, };
	int ret = 0;
	size_t i = 0;

	while (count) {
		if (read_bit(instance, position) == BIT_1)
			ret += value[i];
		count--;
		position++;
		i++;
	}

	return ret;
}

/**
 * Extract the time zone for the next minute
 * @param[in] instance data collection
 * @return 1 for CEST, 2 for CET
 */
static int check_timezone(struct usbdcf_data *instance)
{
	int lc;

	lc = decode_bcd(instance, 17, 2);
	if ((lc == 0) || (lc == 3))
		return -1;

	return lc;
}

/**
 * Read the time zone change bit (bit 16)
 * @param[in] instance data collection
 * @return 0 or 1
 */
static int check_timezone_change(struct usbdcf_data *instance)
{
	return read_bit(instance, 16U);
}

/**
 * Read the leap second add/remove flag (bit 19)
 * @param[in] instance data collection
 * @return 0 or 1
 */
static int check_leap_second(struct usbdcf_data *instance)
{
	return read_bit(instance, 19);
}

/**
 * @param[in] instance data collection
 * @return -1 in case of failure, else next minute
 */
static int check_minute(struct usbdcf_data *instance)
{
	if (checksum(instance, 21, 8) == 0)
		return decode_bcd(instance, 21, 7);

	return -1;
}

/**
 * @param[in] instance data collection
 * @return -1 in case of failure, else next hour
 */
static int check_hour(struct usbdcf_data *instance)
{
	if (checksum(instance, 29, 7) == 0)
		return decode_bcd(instance, 29, 6);

	return -1;
}

/**
 * @param[in] instance data collection
 * @return -1 in case of failure, else next day of month
 */
static int check_day_of_month(struct usbdcf_data *instance)
{
	if (check(instance, 36, 6) == 0)
		return decode_bcd(instance, 36, 6);

	return -1;
}

/**
 * @param[in] instance data collection
 * @return -1 in case of failure, else month
 */
static int check_month(struct usbdcf_data *instance)
{
	if (check(instance, 45, 5) == 0)
		return decode_bcd(instance, 45, 5);

	return -1;
}

/**
 * @param[in] instance data collection
 * @return -1 in case of failure, else next year
 */
static int check_year(struct usbdcf_data *instance)
{
	if (checksum(instance, 36, 23) == 0)
		return decode_bcd(instance, 50, 8);

	return -1;
}

/**
 * Handle the time component of the bit stream
 * @param[in] instance data collection
 * @param[out] overflow 1 if the time overflows and must be accounted by the date
 * @return 1 if a new time stamp is available
 */
static int decode_time(struct usbdcf_data *instance, int *overflow)
{
	int tmp, rc = 0;

	*overflow = 0;

	instance->next_time.summer_time = -1;	/* unknown yet */

	switch (instance->time_reliability) {
	default:
	case TIME_UNKNOWN:
		instance->sync_status &= ~TIME_SYNCED;
		instance->next_time.minute = check_minute(instance);
		if (instance->next_time.minute == -1)
			break;
		instance->next_time.hour = check_hour(instance);
		if (instance->next_time.hour == -1)
			break;
		instance->time_reliability = TIME_IN_SYNC_1;
		break;

	case TIME_IN_SYNC_1:
		tmp = check_minute(instance);
		if (tmp == -1 || tmp != (instance->next_time.minute + 1)) {
			instance->time_reliability = TIME_UNKNOWN;
			break;
		}
		instance->next_time.minute = tmp;

		tmp = check_hour(instance);
		if (tmp == -1 || tmp != instance->next_time.hour) {
			instance->time_reliability = TIME_UNKNOWN;
			break;
		}
		instance->time_reliability = TIME_IN_SYNC_2;
		break;

	case TIME_IN_SYNC_2:
		tmp = check_minute(instance);
		if (tmp == -1 || tmp != (instance->next_time.minute + 1)) {
			instance->time_reliability = TIME_IN_SYNC_1;
			break;
		}
		instance->next_time.minute = tmp;

		tmp = check_hour(instance);
		if (tmp == -1 || tmp != instance->next_time.hour) {
			instance->time_reliability = TIME_IN_SYNC_1;
			break;
		}
		instance->time_reliability = TIME_KNOWN;
		break;

	case TIME_KEEP_AN_EYE:
		instance->next_time.minute = check_minute(instance);
		if (instance->next_time.minute == -1) {
			/* try to repair the minute */
			instance->next_time.minute = instance->current_time.minute + 1;
			if (instance->next_time.minute > 59)
				instance->next_time.minute = 0;
			instance->time_reliability = TIME_IN_SYNC_2;
			instance->sync_status &= ~TIME_SYNCED;
			syslog(LOG_NOTICE, "Time's state changed to UNSYNCED");
		}

		if (instance->next_time.minute < instance->current_time.minute)
			*overflow = 1;

		instance->next_time.hour = check_hour(instance);
		if (instance->next_time.hour == -1) {
			/* try to repair the hour */
			instance->next_time.hour = instance->current_time.hour + *overflow;
			if (instance->next_time.hour > 23)
				instance->next_time.hour = 0;
			instance->time_reliability = TIME_IN_SYNC_2;
			instance->sync_status &= ~TIME_SYNCED;
			syslog(LOG_NOTICE, "Time's state changed to UNSYNCED");
		}

		if (instance->next_time.hour < instance->current_time.hour)
			*overflow = 1;
		else
			*overflow = 0;
		if (instance->time_reliability == TIME_KEEP_AN_EYE) {
			instance->time_reliability = TIME_KNOWN;
			rc = 1; /* okay, we got a new time */
			tmp = check_timezone(instance);
			if (tmp != -1) {
				if (tmp == 1)
					instance->next_time.summer_time = 1;
				else
					instance->next_time.summer_time = 0;
			}

			if (check_leap_second(instance) == BIT_1)
				instance->leap_second_count++;
		}
		break;

	case TIME_KNOWN:
		instance->next_time.minute = check_minute(instance);
		if (instance->next_time.minute == -1) {
			/* try to repair the minute */
			instance->next_time.minute = instance->current_time.minute + 1;
			if (instance->next_time.minute > 59)
				instance->next_time.minute = 0;
			instance->time_reliability = TIME_KEEP_AN_EYE;
			syslog(LOG_NOTICE, "Time's internal state changed to DATE_KEEP_AN_EYE");
		}

		if (instance->next_time.minute < instance->current_time.minute)
			*overflow = 1;

		instance->next_time.hour = check_hour(instance);
		if (instance->next_time.hour == -1) {
			/* try to repair the hour */
			instance->next_time.hour = instance->current_time.hour + *overflow;
			if (instance->next_time.hour > 23)
				instance->next_time.hour = 0;
			instance->time_reliability = TIME_KEEP_AN_EYE;
			syslog(LOG_NOTICE, "Time's internal state changed to DATE_KEEP_AN_EYE");
		}

		if (instance->next_time.hour < instance->current_time.hour)
			*overflow = 1;
		else
			*overflow = 0;

		if (instance->time_reliability == TIME_KNOWN) {
			if (!(instance->sync_status & TIME_SYNCED))
				syslog(LOG_INFO, "Time's state changed to SYNCED");
			instance->sync_status |= TIME_SYNCED;
		}
		rc = 1; /* okay, we got a new time */
		tmp = check_timezone(instance);
		if (tmp != -1) {
			if (tmp == 1)
				instance->next_time.summer_time = 1;
			else
				instance->next_time.summer_time = 0;
		}

		if (check_leap_second(instance) == BIT_1)
			instance->leap_second_count++;

		break;
	}

	return rc;
}

/**
 * Handle the date component of the bit stream
 * @param[in] instance data collection
 * @param[in] overflow 1 if the time of day overflows
 *
 * FIXME: Date uses one parity bit for all parts (32 bits from bit 36 on)
 */
static int decode_date(struct usbdcf_data *instance, int overflow)
{
	int tmp, rc = 0;

	switch (instance->date_reliability) {
	default:
	case DATE_UNKNOWN:
		instance->sync_status &= ~DATE_SYNCED;
		/*
		 * We know nothing about the date. We need at least
		 * one complete date set to do any further tests.
		 */
		instance->next_time.day = check_day_of_month(instance);
		if (instance->next_time.day < 0)
			break;
		instance->next_time.month = check_month(instance);
		if (instance->next_time.month < 0)
			break;
		instance->next_time.year = check_year(instance);
		if (instance->next_time.year < 0)
			break;
		/* one full set received */
		instance->date_reliability = DATE_IN_SYNC_1;
		syslog(LOG_NOTICE, "Date's internal state changed to DATE_IN_SYNC_1");
		break;

	case DATE_IN_SYNC_1:
		/*
		 * at least one date set we already received. We are
		 * waiting for a second one, to check the information.
		 * This will fail, when someone starts near midnight...
		 * At this point of time it will take more time to sync in.
		 */
		tmp = check_day_of_month(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.day) {
			instance->date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}

		tmp = check_month(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.month) {
			instance->date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}

		tmp = check_year(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.year) {
			instance->date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}
		instance->date_reliability = DATE_IN_SYNC_2;
		syslog(LOG_NOTICE, "Date's internal state changed to DATE_IN_SYNC_2");
		break;

	case DATE_IN_SYNC_2:
		/*
		 * at least two date sets we already received. We are
		 * waiting for a third one, to check the information.
		 * This will fail, when someone starts near midnight...
		 * But it only will take more time to sync in.
		 */
		tmp = check_day_of_month(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.day) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}

		tmp = check_month(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.month) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}

		tmp = check_year(instance);
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != instance->next_time.year) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}
		instance->date_reliability = DATE_KNOWN;
		syslog(LOG_NOTICE, "Date's internal state changed to DATE_KNOWN");
		break;

	case DATE_KEEP_AN_EYE:
		/*
		 * We are synced and trust our stored date. But the new data
		 * seems instable, so we must keep an eye on it, if we can
		 * still trust on it. If the new data again is faulty, we
		 * fall back to an earlier check state.
		 */
		instance->next_time.day = check_day_of_month(instance);
		if (instance->next_time.day == -1) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			instance->sync_status &= ~DATE_SYNCED;
			syslog(LOG_NOTICE, "Date's state changed to UNSYNCED");
		}

		instance->next_time.month = check_month(instance);
		if (instance->next_time.month == -1) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			instance->sync_status &= ~DATE_SYNCED;
			syslog(LOG_NOTICE, "Date's state changed to UNSYNCED");
		}

		instance->next_time.year = check_year(instance);
		if (instance->next_time.year == -1) {
			instance->date_reliability = DATE_IN_SYNC_1; /* fall back */
			instance->sync_status &= ~DATE_SYNCED;
			syslog(LOG_NOTICE, "Date's state changed to UNSYNCED");
		}
		/* Seems okay again now. Back to known state */
		if (instance->date_reliability == DATE_KEEP_AN_EYE) {
			instance->date_reliability = DATE_KNOWN;
			rc = 1;	/* okay we got a new date */
			syslog(LOG_NOTICE, "Date's internal state changed to DATE_KNOWN");
		}
		break;

	case DATE_KNOWN:
		/*
		 * It seems we know the current date. Start to
		 * trust the information. And fix it in case of failures.
		 */
		instance->next_time.day = check_day_of_month(instance);
		if (instance->next_time.day == -1) {
			/* try to repair the day of month */
			instance->date_reliability = DATE_KEEP_AN_EYE;
			syslog(LOG_NOTICE, "Date's internal state changed to DATE_KEEP_AN_EYE");
			if (overflow != 0) {
				if (instance->current_time.day < 28)
					instance->next_time.day = instance->current_time.day + 1;
				else {
					/* dilemma: How to know when this day of month overflows? */
					/* we need the current month (and year) to decide! */
					instance->sync_status &= ~DATE_SYNCED;
					syslog(LOG_NOTICE, "Date's state changed to UNSYNCED");
				}
			} else
				instance->next_time.day = instance->current_time.day;
		}

		if (instance->next_time.day < instance->current_time.day)
			overflow = 1;
		else
			overflow = 0;

		instance->next_time.month = check_month(instance);
		if (instance->next_time.month == -1) {
			instance->date_reliability = DATE_KEEP_AN_EYE;
			syslog(LOG_NOTICE, "Date's internal state changed to DATE_KEEP_AN_EYE");
			/* try to repair the month */
			instance->next_time.month = instance->current_time.month + overflow;
			if (instance->next_time.month > 12)
				instance->next_time.month = 12;
		}

		if (instance->next_time.month < instance->current_time.month)
			overflow = 1;
		else
			overflow = 0;

		instance->next_time.year = check_year(instance);
		if (instance->next_time.year == -1) {
			instance->date_reliability = DATE_KEEP_AN_EYE;
			syslog(LOG_NOTICE, "Date's internal state changed to DATE_KEEP_AN_EYE");
			/* try to repair the year */
			instance->next_time.year = instance->current_time.year + overflow;
		}

		if (instance->date_reliability == DATE_KNOWN) {
			if (!(instance->sync_status & DATE_SYNCED))
				syslog(LOG_INFO, "Date's state changed to SYNCED");
			instance->sync_status |= DATE_SYNCED;
		}

		rc = 1;	/* okay we got a new date */
		break;
	}

	return rc;
}

/**
 * @param[in] instance data collection
 */
static void decode_stream(struct usbdcf_data *instance)
{
	int overflow;
	int time_avail, date_avail;

	time_avail = decode_time(instance, &overflow);
	date_avail = decode_date(instance, overflow);

	if (time_avail != 0 && date_avail != 0)
		instance->new_time_available = 1;
}

/**
 * Sort in the next bit
 * @param[in] instance data collection
 * @param[in] value Code of the next bit
 */
static void report_bit(struct usbdcf_data *instance, unsigned value)
{
	if (value == BIT_SYNC) {
		if (instance->current_index < 59) {
#ifdef DEBUG
			syslog(LOG_INFO, "Sync for incomplete dataset");
#endif
			instance->current_index = 0;	/* sort out incomplete stream */
		} else {
			decode_stream(instance);
#ifdef DEBUG
			syslog(LOG_INFO, "Sync for complete dataset");
#endif
			instance->current_index = 0;
		}
		return;
	}
	store_next_bit(instance, value);
}

/**
 * Report any new data set read from the clock
 * @param[inout] instance all we need to know about our time source
 *
 * We are feeding the DCF data into the decoder. If it reports a new timestamp
 * we must wait for two reference events. The second one corresponds to the
 * new timestamp.
 */
void feed_data(struct usbdcf_data *instance)
{
	if (instance->data.flags) {
		/*
		 * Handle the DCF bits first, then the reference time
		 * to ensure correct handling if both events are reported in
		 * the same dataset
		 */
		if (instance->data.flags & 0x02)
			report_bit(instance, BIT_UNKNOWN);	/* we lost at least one bit */
		if (instance->data.flags & 0x01)
			report_bit(instance, instance->data.bit);

		if (instance->data.flags & 0x10) {
			feed_next_second(instance);

			if (instance->report_new_time != 0) {
				feed_valid_time(instance);
				instance->report_new_time = 0;
			}

			if (instance->new_time_available != 0) {
				instance->current_time = instance->next_time;
				/* when the next reference second reaches, report this new time */
				instance->report_new_time = 1;
				instance->new_time_available = 0;
			}
		}
	}
}

/**
 * @param[in] instance data collection
 */
int init_dcf77_decoder(struct usbdcf_data *instance)
{
	struct timeval current_time;

	/*
	 * in order to make mktime() work in an expected manner, we must
	 * delete the timezone information. This ensures we can provide
	 * UTC in a struct tm and will get UTC as the result of the conversion
	 */
	setenv("TZ", "", 1);
	tzset();

	/*
	 * Let's start with the current system time. Even if there is no
	 * valid DCF bit stream, we can use the "one pulse per second" to
	 * avoid any drift of system's time.
	 * FIXME: Useless feature. Most of the time it confuses
	 * the NTPD more than it helps. Should be removed.
	 */
	gettimeofday(&current_time, NULL);
	gmtime_r(&current_time.tv_sec, &instance->reference_time);

	/* we don't know the correct time yet */
	instance->clock_is_set = 0;

	return 0;
}

/**
 * @param[in] instance data collection
 */
void exit_dcf77_decoder(struct usbdcf_data *instance)
{
}
