/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#include <pthread.h>
#include <sys/time.h>

#ifdef DEBUG
# define debugOut(x,y...) syslog(LOG_DEBUG,x,## y)
#else
# define debugOut(x,y...)
#endif

/**
 * Data format the USB based DCF77 receiver uses
 */
struct communication {
	uint8_t flags;
	uint8_t bit;
	uint16_t stamp_offset;
	uint16_t cur_phase;
	uint16_t cur_interval;
} __attribute__((packed));

/* bit markers encoded by the USB DCF77 clock */

#define BIT_UNKNOWN 15
#define BIT_SYNC 8
#define BIT_1 1
#define BIT_0 0

struct encoded_time {
	int day, hour, minute, second;
	unsigned long millisecond;
	struct timeval time_stamp;
	int day_of_week, month, year;
	int summer_time;
};

struct usbdcf_data {
	void *usb_device;

	/* USB communication part */
	struct timeval pre_time_stamp;
	struct timeval post_time_stamp;
	struct communication data;	/** USB data buffer */

	/* DCF77 decoder part */
	unsigned char rec_buf[61];
	int current_index;		/**< pointer in rec_buf and also the current second */
	int new_time_available;		/**< not zero, if the decoder provides a new timestamp */
	int report_new_time;		/**< not zero, when we should report a new time to the ntpd */
	unsigned sync_status;
	unsigned time_reliability;	/** state machine to check the data stream. Can be DATE_UNKNOWN, DATE_IN_SYNC_1, DATE_IN_SYNC_2, DATE_KEEP_AN_EYE, DATE_KNOWN */
	unsigned date_reliability;	/** state machine to check the data stream. can be TIME_UNKNOWN, TIME_IN_SYNC_1, TIME_IN_SYNC_2, TIME_KEEP_AN_EYE, TIME_KNOWN */
	struct encoded_time next_time;
	struct encoded_time current_time;

	struct tm reference_time;	/** internal current clock, based on the DCF signal, but counted in UTC */
	unsigned usec_offset;		/** offset from the reference time */
	struct timeval read_time_stamp;	/** system time the clock was read */
	int leap_second;		/** not 0 if we expect a leap second at the end of this hour */
	int leap_second_count;		/** count of set leap second markers in the bit stream */
	int clock_is_set;

	void *ntp_connector;
};

/* from dcf77_decoder.c */
extern int init_dcf77_decoder(struct usbdcf_data*);
extern void feed_data(struct usbdcf_data*);
extern void exit_dcf77_decoder(struct usbdcf_data*);

/* from usb_connector.c */
extern int open_usb_clock(void**);
extern int reconnect_usb_clock(void**, int);
extern void close_usb_clock(void*);
extern int read_usb_clock(void*, void*,int, int);
extern const char *error_usb_clock(const int);

/* from ntpdconnector.c */
extern int init_ntp_connection(struct usbdcf_data*, int);
extern int report_time(struct usbdcf_data*);
extern void exit_ntp_connection(struct usbdcf_data*);

/* from clockread.c */
extern void die_hard(void);

/* ----------------- Identification of our clock at the USB --------------- */

#define USB_VENDOR_ID 0x16C0
#define USB_DEVICE_ID 0x05DF
#define USB_VENDOR_STRING "kreuzholzen.de"
#define USB_DEVICE_STRING "DCF77-Clock"

/* this report the USB clock can handle */
#define USBRQ_HID_GET_REPORT 0x01
