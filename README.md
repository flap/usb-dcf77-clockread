Note: English language variant see below.

DCF77 USB-Empfänger: Anbindung an den NTP Daemon
================================================

Das Quellpaket des NTP-Daemons bringt viele Uhrentreiber mit. Mein erster Gedanke
war daher, dass es nicht so schwer sein kann, dort ein geeignetes Beispiel zum
Abschreiben zu finden. Prinzipiell stimmt das auch. Leider habe ich aber ein paar
Nebenbedingungen zu erfüllen, die wiederum der NTP-Daemon nicht erfüllt.
Beispielsweise muss ich meine Uhr zweimal pro Sekunde abfragen, damit ich weder
das DCF77-Sekunden-Signal, noch das Sekunden-Referenz-Signal verpasse. Der
NTP-Daemon ruft aber die Treiber nicht mit dieser Rate auf. Aber es gibt ja noch
Threads. Davon riet man mir aber ab, weil ich dann der erste wäre, der den NTP-Daemon
mit Threads unter POSIX betreiben würde. Und dieses Neuland wollte ich dann
nicht auch noch betreten. (Nachtrag 2020: Das kann inzwischen alles anders sein).

Was blieb, war der SHM-Treiber. Ein separates Programm trägt die Uhrzeit zusammen
und liefert diese an den NTP-Daemon über einen *shared memory* Bereich. Somit
kann ich in meinem Host-seitigen Programm tun und lassen was ich will um die
Uhrzeit über USB zu ermitteln und trotzdem benutzt der NTP-Daemon die gelieferte
Zeit, ohne diesen irgendwie ändern zu müssen.

Quellen und Konfiguration
-------------------------

Das hier sind die Quellen des SHM-Treibers (clockread). Alles was jetzt noch zu
tun ist, ist den NTPD zu starten und vorher in seiner Konfigurationsdatei
``/etc/ntpd.conf`` den externen Treiber hinzuzufügen::

  [...]
  server 127.127.28.0 mode 0 prefer
  fudge 127.127.28.0 stratum 0
  [...]

Danach genügt es, die USB-Uhr anzustecken und zu schauen, ob der Kernel gewillt
ist, damit zu arbeiten. Es sind übrigens keine Kernel-Treiber notwendig. Die
gesamte Kommunikation wird über die *libusb* abgewickelt. Mein Kernel meldet
folgendes::

    [...]
    usb 5-1: new low speed USB device using uhci_hcd and address 7
    usb 5-1: configuration #1 chosen from 1 choice
    hiddev96: USB HID v1.01 Device [kreuzholzen.de DCF77-Clock] on usb-0000:00:1d.3-1

Um die Daten der DCF77-Uhr auszuwerten und dem NTPD als Quelle zuzuführen genügt
es nun das Programm *clockread* zu starten. Dieses Programm gibt via Syslog
einige Hinweise über seinen Zustand aus und der NTPD kann mittels ntpq-Kommando
dabei beobachtet werden, ob und wie er die Zeit aus dem shared memory Treiber
verwendet::

    [me@host]~$ ntpq -p localhost
         remote           refid      st t when poll reach   delay   offset  jitter
    ==============================================================================
     LOCAL(0)        .LOCL.          10 l   57   64  377    0.000    0.000   0.004
    *SHM(0)          .SHM.            0 l   31   64  377    0.000    0.486   0.287


DCF77 USB receiver: Connection to the NTP daemon
================================================

The source archive of the NTPD daemon comes with many clock drivers. I was sure
to find one in the list to have an example to derive my own driver from. It was
nearly true. But my clock device has some special conditions. For example the
clock has to be queried two times a second. But the NTPD calls the clock driver
less frequent. Okay, could be handled in a thread I thought. But the POSIX NTPD
implementation didn't support threads. And I didn't want to break new ground...

Solution was the shared memory clock driver. This clock driver runs as a discrete
process and forwards the time via a piece of memory, shared with the NTPD. With
this approach the NTPD needs no modification and my clock driver can use any
POSIX feature I want (or need).

Sources and configuration
-------------------------

This are the shared memory clock driver sources (clockread). All you need to do
now is to start the NTPD, but add these lines to its configuration file
``/etc/ntpd.conf`` first::

    [...]
    server 127.127.28.0 mode 0 prefer
    fudge 127.127.28.0 stratum 0
    [...]

After that just plug in the USB clock and look if the kernel discover it. By
the way: To use this USB clock there is no need for any kind of kernel driver.
Everything is handled within userland with the help of *libusb*. When plugging
in the USB clock, my kernel states::

    [...]
    usb 5-1: new low speed USB device using uhci_hcd and address 7
    usb 5-1: configuration #1 chosen from 1 choice
    hiddev96: USB HID v1.01 Device [kreuzholzen.de DCF77-Clock] on usb-0000:00:1d.3-1

To decode the DCF77 bits and events and to forward the time to the NTPD just
start the program *clockread* now. It gives some information about its internal
state via syslog. The state of the NTPD can be queried with the ntpq command if
it uses the shared memory clock driver::

    [me@host]~$ ntpq -p localhost
         remote           refid      st t when poll reach   delay   offset  jitter
    ==============================================================================
     LOCAL(0)        .LOCL.          10 l   57   64  377    0.000    0.000   0.004
    *SHM(0)          .SHM.            0 l   31   64  377    0.000    0.486   0.287
